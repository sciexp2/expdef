SciExp²-ExpDef documentation contents
=====================================


:Release: |version|
:Date: |today|

Contents:

.. toctree::
   :numbered:
   :maxdepth: 2

   introduction.rst
   user_guide.rst
   reference.rst

.. toctree::
   :hidden:

   news.rst
   news-old.rst


Indices and tables
==================

* :ref:`news`
* :ref:`genindex`
* :ref:`modindex`
