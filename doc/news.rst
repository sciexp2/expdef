.. _news:

Changes in SciExp²-ExpDef
=========================

Here's a brief description of changes introduced on each version.


2.1.7
-----

.. rubric:: External compatibility breaks

.. rubric:: New features

.. rubric:: Improvements

.. rubric:: Bug fixes

* Fix concurrency bugs in `sciexp2.expdef.experiments.with_exp` and `sciexp2.expdef.experiments.with_exp_tpl`.

.. rubric:: Documentation

.. rubric:: Internals


2.1.6
-----

.. rubric:: External compatibility breaks

.. rubric:: New features

.. rubric:: Improvements

.. rubric:: Bug fixes

* Fix concurrency bugs in `Experiments.generate` and `Experiments.generate_jobs`.
* Fix file mode settings in `Experiments.generate` and `Experiments.generate_jobs`.

.. rubric:: Documentation

.. rubric:: Internals


2.1.5
-----

.. rubric:: External compatibility breaks

.. rubric:: New features

.. rubric:: Improvements

.. rubric:: Bug fixes

* Fix `parallel` in `sciexp2.expdef.experiments.ExperimentsView`.

.. rubric:: Documentation

.. rubric:: Internals


2.1.4
-----

.. rubric:: External compatibility breaks

.. rubric:: New features

.. rubric:: Improvements

.. rubric:: Bug fixes

* Fix `parallel` in `sciexp2.expdef.experiments.ExperimentsView`.

.. rubric:: Documentation

.. rubric:: Internals


2.1.3
-----

.. rubric:: External compatibility breaks

* Remove package `sciexp2.common.parallel`.

.. rubric:: New features

* Allow parallelization in `sciexp2.expdef.experiments.Experiments.generate` and  `sciexp2.expdef.experiments.Experiments.generate_jobs` (enabled by default).

.. rubric:: Improvements

* Make prints in `sciexp2.expdef.experiments.Experiments.execute` play nice with progress bars.

.. rubric:: Bug fixes

.. rubric:: Documentation

.. rubric:: Internals

* Replace `sciexp2.common.parallel` with `concurrent`.

* Make progress objects compatible with regular printing by adding `sciexp2.common.progress.Progress.write` and  `sciexp2.common.progress.Null.write` to wrap `tqdm.tqdm.write`.

* Allow concurrent execution of `sciexp2.common.utils.assert_dir`.

* Fix file generation errors in `sciexp2.expdef`.


2.1.2
-----

.. rubric:: External compatibility breaks

* Rename `Instance.auto_translation` to `Instance.auto_translate`.

.. rubric:: New features

.. rubric:: Improvements

.. rubric:: Bug fixes

* Fix `Instance` auto-translation when elements have no currently available translation.

.. rubric:: Documentation

.. rubric:: Internals


2.1.1
-----

.. rubric:: External compatibility breaks

.. rubric:: New features

.. rubric:: Improvements

.. rubric:: Bug fixes

* Fix `Instance` auto-translation cache with empty `InstanceGroup`.

.. rubric:: Documentation

.. rubric:: Internals


2.1.0
-----

.. rubric:: External compatibility breaks

* Remove `sciexp2.expdef.system.SEARCH_PATH` in favor of Python's existing module import engine.

.. rubric:: New features

* Add context manager `Instance.auto_translation`.

.. rubric:: Improvements

* Accelerate file, text and command generation in `sciexp2.expdef.experiments` by using `Instance.auto_translation`.
* Auto-export variables used in any dependency for `sciexp2.expdef.experiments.Experiments.generate_jobs`.

.. rubric:: Bug fixes

* Fix variable export selection when serializing `Instance` objects.
* Fix expansion of `expdef.experiments.with_exp` and `expdef.experiments.with_exp_tpl` during text generation.

.. rubric:: Documentation

.. rubric:: Internals


2.0.14
------

.. rubric:: External compatibility breaks

.. rubric:: New features

.. rubric:: Improvements

.. rubric:: Bug fixes

* Fix error message when `Experiment.execute` has duplicate instances.

.. rubric:: Documentation

.. rubric:: Internals


2.0.13
------

.. rubric:: External compatibility breaks

.. rubric:: New features

.. rubric:: Improvements

.. rubric:: Bug fixes

* Fix switch to Python3.

.. rubric:: Documentation

.. rubric:: Internals


2.0.12
------

.. rubric:: External compatibility breaks

.. rubric:: New features

.. rubric:: Improvements

.. rubric:: Bug fixes

* Update copyright.
* Fully switch to Python 3.

.. rubric:: Documentation

.. rubric:: Internals


2.0.11
------

.. rubric:: External compatibility breaks

.. rubric:: New features

.. rubric:: Improvements

.. rubric:: Bug fixes

* Fix `sciexp2.common.utils.str2num` compatibility with Python 3.

.. rubric:: Documentation

.. rubric:: Internals


2.0.10
------

.. rubric:: External compatibility breaks

* Remove obsolete `sciexp2.common.utils.copy_path_maybe`.

.. rubric:: New features

.. rubric:: Improvements

.. rubric:: Bug fixes

* Fix string encoding in ``setup.py``.

.. rubric:: Documentation

.. rubric:: Internals


2.0.9
-----

.. rubric:: External compatibility breaks

.. rubric:: New features

.. rubric:: Improvements

.. rubric:: Bug fixes

* Fix parameter recombination in `sciexp2.expdef.experiments.Experiments.params` (was failing when redefining the values of an existing variable).

.. rubric:: Documentation

.. rubric:: Internals


2.0.8
-----

.. rubric:: External compatibility breaks

.. rubric:: New features

.. rubric:: Improvements

.. rubric:: Bug fixes

.. rubric:: Documentation

.. rubric:: Internals

* Fix packaging.


2.0.7
-----

* Reupload version.


2.0.6
-----

.. rubric:: External compatibility breaks

.. rubric:: New features

.. rubric:: Improvements

.. rubric:: Bug fixes

.. rubric:: Documentation

.. rubric:: Internals

* Fix packaging.
* Improve release procedures.


2.0.5
-----

.. rubric:: External compatibility breaks

.. rubric:: New features

.. rubric:: Improvements

.. rubric:: Bug fixes

* Fix use of relative path templates in `sciexp2.common.utils.find_files`.

.. rubric:: Documentation

.. rubric:: Internals

* Fix packaging.


2.0.4
-----

.. rubric:: External compatibility breaks

.. rubric:: New features

.. rubric:: Improvements

.. rubric:: Bug fixes

* Properly export top-level modules.
* Fix filter management in `~sciexp2.expdef.launcher.Launcher`.
* Fix exception reporting in :program:`launcher`.

.. rubric:: Documentation

.. rubric:: Internals


2.0.3
-----

.. rubric:: External compatibility breaks

.. rubric:: New features

.. rubric:: Improvements

.. rubric:: Bug fixes

.. rubric:: Documentation

.. rubric:: Internals

* Fix file generation when destination file already exists.


2.0.2
-----

.. rubric:: External compatibility breaks

.. rubric:: New features

.. rubric:: Improvements

.. rubric:: Bug fixes

.. rubric:: Documentation

.. rubric:: Internals

* Minor release fixes and improvements.


2.0.1
-----

.. rubric:: External compatibility breaks

.. rubric:: New features

.. rubric:: Improvements

.. rubric:: Bug fixes

.. rubric:: Documentation

* Update pypi package description and development status.

.. rubric:: Internals


2.0.0
-----

.. rubric:: External compatibility breaks

* Move top-level modules to `sciexp2.expdef`.
* Rename ``sciexp2.expdef.launchgen`` as `sciexp2.expdef.experiments`.
* Rename ``sciexp2.expdef.experiments.Launchgen`` as `sciexp2.expdef.experiments.Experiments`.
* Rewrite `sciexp2.common.utils.find_files` to use `sciexp2.common.text`.
* Drop ``contents`` attribute in `sciexp2.expdef.experiments.Experiments`.
* Rename ``select`` and ``select_inverse`` methods to `sciexp2.expdef.experiments.Experiments.view` and `sciexp2.expdef.experiments.ExperimentsView.view_inverse`.
* Refactor `sciexp2.expdef.experiments.Experiments.execute` to use `sciexp2.common.text` (closes :issue:`1`).
* Refactor `sciexp2.expdef.experiments.Experiments.params` to avoid translations (closes :issue:`1`).
* Refactor `sciexp2.expdef.experiments.Experiments.translate` and delete ``sciexp2.expdef.experiments.Experiments.expand`` to use `sciexp2.common.text` (closes :issue:`1`).
* Remove ``sciexp2.expdef.experiments.defer`` and ``sciexp2.expdef.experiments.defer_evaluate`` in favor of `sciexp2.expdef.experiments.with_exp`.
* Remove ``sciexp2.expdef.experiments.file_contents`` in favor of `sciexp2.expdef.experiments.with_exp_tpl`.
* Remove ``sciexp2.expdef.experiments.find_files``, ``sciexp2.expdef.experiments.find_SPEC``, and ``sciexp2.expdef.experiments.find_simpoint`` in favor of `sciexp2.expdef.experiments.from_find_files` (closes :issue:`1`).
* Refactor `sciexp2.expdef.experiments.Experiments.pack` to use `sciexp2.common.text` (closes :issue:`1`).
* Refactor `sciexp2.expdef.experiments.Experiments.generate` to use `sciexp2.common.text` (closes :issue:`1`).
* Remove ``sciexp2.expdef.experiments.launchgen`` in favor of `sciexp2.expdef.experiments.Experiments.generate_jobs` to use `sciexp2.common.text` (closes :issue:`1`).
* Port `sciexp2.expdef.templates` to use `sciexp2.common.text` (closes :issue:`1`).
* Port `sciexp2.expdef.system` to use `sciexp2.common.text` (closes :issue:`1`).

.. rubric:: New features

* Added module `sciexp2.common.text`.
* Allow control of repeated commands in `sciexp2.expdef.experiments.Experiments.execute`.
* Add `sciexp2.expdef.experiments.Experiments.params_append` (closes :issue:`1`).
* Add `sciexp2.expdef.experiments.with_exp` and `sciexp2.expdef.experiments.with_exp_tpl` to use `sciexp2.common.text` (closes :issue:`1`).
* Allow dynamic changes to the progress indicators through the ``+=`` operator.
* Add `sciexp2.expdef.experiments.from_function`.
* Add `sciexp2.expdef.experiments.from_find_files` to use `sciexp2.expdef.experiments.from_function` and `sciexp2.common.text` (closes :issue:`1`).

.. rubric:: Improvements

.. rubric:: Bug fixes

.. rubric:: Documentation

.. rubric:: Internals

* Move into a separate package.
* Remove module ``sciexp2.common.varref``.


Older versions
--------------

:ref:`news-old`.
