Reference Guide
===============

.. currentmodule:: sciexp2

.. note::

   Input files for the examples shown in the documentation are available on the root directory of the source distribution.

.. autosummary::
   :toctree: _reference

   expdef.env
   expdef.experiments
   expdef.launcher
   expdef.system
   expdef.system.gridengine
   expdef.system.shell
   expdef.templates
   common.filter
   common.instance
   common.parallel
   common.pp
   common.progress
   common.text
   common.utils
