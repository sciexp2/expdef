Installing
==========

The simplest, system-agnostic, way to install SciExp²-ExpDef is using the package from the official Python Package Index (PyPi [#pypi]_) using :program:`virtualenv` and :program:`pip` [#virtualenv]_. Using :program:`virtualenv` will ensure you have a controlled local installation of a specific version for each of your projects, making sure your scripts always use the same SciExp²-ExpDef version to avoid any possible future version incompatibilities.

1. Install :program:`virtualenv` and :program:`pip`.
2. Create a python package "environment"::

    virtualenv --system-site-packages ~/my-expdef

  You can create as many as you want; e.g., one for each different version of the SciExp²-ExpDef package that you want to test.

  If you want to ignore the python packages installed on the system, you can instead run::

    virtualenv ~/my-expdef

3. Install SciExp²-ExpDef::

     . ~/my-expdef/bin/activate
     pip install sciexp2-expdef

   Or to install a specific version (version ``0.0.0`` in the example)::

     . ~/my-expdef/bin/activate
     pip install "sciexp2-expdef==0.0.0"

.. [#pypi] https://pypi.python.org
.. [#virtualenv] http://www.pip-installer.org/en/latest/installing.html


Running
=======

If you have installed SciExp²-ExpDef using :program:`pip`, just remember to activate your environment before using it::

  . ~/my-expdef/bin/activate
  # programs and paths are now properly set up

.. note::

   Many potentially long operations provide a feedback in the form of progress indicators; you can control whether they are shown through `sciexp2.common.progress.level`.


SciExp²-ExpDef and Make
~~~~~~~~~~~~~~~~~~~~~~~

In you use :program:`make` to automate the execution of your scripts, here is a simple snippet you can add to your ``Makefile`` (the example uses version ``0.0.0`` of SciExp²-ExpDef)::

  all: deps/expdef
        # run our script using expdef
  	( . deps/expdef/bin/activate && /path/to/my/script.py )

  # the ".done" file ensures a partial installation will not count as a success
  deps/expdef: deps/expdef/.done
  	$(RM) -R $@
        mkdir -p $(dir $@)
  	virtualenv --system-site-packages $@
  	( . $@/bin/activate && pip install "sciexp2-expdef==0.0.0" )
  	touch $<


Debugging aids
~~~~~~~~~~~~~~

You can start your scripts with ``ipython --pdb /path/to/my/script.py`` to get into a debugging shell whenever an error occurs [#pdb]_. Sometimes it is also useful to start an IPython shell [#embed]_ somewhere deep in your code to interactively evaluate its current state::

  from IPython import embed
  embed()

Exiting the debugging shell will returning to normal execution.

.. [#pdb] http://ipython.org/ipython-doc/stable/interactive/reference.html#post-mortem-debugging
.. [#embed] http://ipython.org/ipython-doc/stable/interactive/reference.html#embedding-ipython
