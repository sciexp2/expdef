#!/bin/bash -e

if [ $# -ne 1 ]; then
    echo "Usage: `basename $0` <version>"
    exit 1
fi

version=${1//./\.}
src_version=${version%-*}

cd `dirname $0`

if git tag -l | grep -F "^v${version} " >/dev/null; then
    echo "Version already tagged in the repository"
    exit 1
fi

if ! python3 ./launcher -v >/dev/null 2>&1; then
    echo "Could not run launcher program"
    exit 1
fi

if ! python3 ./launcher -v 2>&1 | grep "^${src_version}$" >/dev/null; then
    echo "You must update ./sciexp2/expdef/__init__.py"
    exit 1
fi

if ! grep "^${src_version}$" ./doc/news.rst >/dev/null; then
    echo "You must update ./doc/news.rst"
    exit 1
fi

branch=`git branch | grep "^\*" | cut -f 2 -d " "`
if [ "$branch" != "master" ]; then
    echo "error: You must be in master branch"
    exit 1
fi

if [ -n "`git status --porcelain`" ]; then
    echo "error: You have local changes that need committing"
    exit 1
fi

set -e

# make sure it passes all tests
python3 ./run-tests.py

git tag v$1
git push
git push --tags
