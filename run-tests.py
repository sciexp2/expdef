#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import subprocess
import sys
import yaml

base = os.path.dirname(sys.argv[0])
gitlab_ci = os.path.join(base, ".gitlab-ci.yml")

with open(gitlab_ci, "r") as f:
    data = yaml.load(f, yaml.Loader)

stages = [stage for stage in data["stages"]
          if stage != "release"]

tests = [test for test in data.keys()
         if test in stages or (isinstance(data[test], dict) and data[test].get("stage", None) in stages)]

for test in tests:
    allow_failure = data[test].get("allow_failure", None)
    cmd = ["gitlab-runner", "exec", "docker", test]
    if allow_failure:
        subprocess.call(cmd)
    else:
        subprocess.check_call(cmd)
