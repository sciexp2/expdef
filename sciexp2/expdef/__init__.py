#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__     = "Lluís Vilanova"
__copyright__  = "Copyright 2008-2024, Lluís Vilanova"
__license__    = "GPL version 3 or later"


__version_info__ = (2, 1, 7)
__version__ = ".".join([str(i) for i in __version_info__])

__all__ = [
    "env", "experiments", "launcher", "system", "templates",
]
