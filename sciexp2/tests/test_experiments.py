#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#pylint: skip-file

"""Data creation tests."""

__author__ = "Lluís Vilanova"
__copyright__ = "Copyright 2017-2023, Lluís Vilanova"
__license__ = "GPL version 3 or later"


import os
import pytest
from sciexp2.common.instance import InstanceGroup, Instance
from sciexp2.common.text import VariableError
from sciexp2.common import utils
from sciexp2.expdef.env import *
from sciexp2.expdef.templates import TemplateError
import tempfile


def test_basics():
    e = Experiments()
    assert repr(e) == "Experiments()"
    assert len(e) == 0
    assert list(e) == []

    e = Experiments(out="test")
    assert repr(e) == "Experiments(out='test')"

    e = Experiments(dereference=True)
    assert repr(e) == "Experiments(dereference=True)"
    assert len(e) == 0
    assert list(e) == []

    e = Experiments(dereference=False)
    assert repr(e) == "Experiments()"
    assert len(e) == 0
    assert list(e) == []

    e.params(a=range(2))
    assert len(e) == 2
    assert list(e) == list(InstanceGroup([{'a': 0}, {'a': 1}]))
    assert isinstance(e[0], Instance)
    assert e[0] == Instance({'a': 0})

    with e.view("a == 0") as v, \
         v.view_inverse() as i:
        assert len(v) == 1
        assert list(v) == list(InstanceGroup([{'a': 0}]))
        assert isinstance(v[0], Instance)
        assert v[0] == Instance({'a': 0})

        assert len(i) == 1
        assert list(i) == list(InstanceGroup([{'a': 1}]))
        assert isinstance(i[0], Instance)
        assert i[0] == Instance({'a': 1})

    e.params(a=1)
    assert isinstance(e[0], Instance)


def test_params():
    e = Experiments()

    e.params(a=range(2), b=range(2))
    assert list(e) == list(InstanceGroup([
        {'a': 0, 'b': 0},
        {'a': 0, 'b': 1},
        {'a': 1, 'b': 0},
        {'a': 1, 'b': 1},
    ]))

    e.params_append(c=0)
    assert list(e) == list(InstanceGroup([
        {'a': 0, 'b': 0},
        {'a': 0, 'b': 1},
        {'a': 1, 'b': 0},
        {'a': 1, 'b': 1},
        {'c': 0},
    ]))

    e.params("d % 2 == 0",
             d=range(4))
    assert list(e) == list(InstanceGroup([
        {'a': 0, 'b': 0, 'd': 0},
        {'a': 0, 'b': 0, 'd': 2},
        {'a': 0, 'b': 1, 'd': 0},
        {'a': 0, 'b': 1, 'd': 2},
        {'a': 1, 'b': 0, 'd': 0},
        {'a': 1, 'b': 0, 'd': 2},
        {'a': 1, 'b': 1, 'd': 0},
        {'a': 1, 'b': 1, 'd': 2},
        {'c': 0, 'd': 0},
        {'c': 0, 'd': 2},
    ]))

    with e.view("a == b") as v, v.view_inverse() as i:
        v.params("d == 0", e=10)
        i.params(e=20)
    assert list(e) == list(InstanceGroup([
        {'a': 0, 'b': 0, 'd': 0, 'e': 10},
        {'a': 0, 'b': 1, 'd': 0, 'e': 20},
        {'a': 0, 'b': 1, 'd': 2, 'e': 20},
        {'a': 1, 'b': 0, 'd': 0, 'e': 20},
        {'a': 1, 'b': 0, 'd': 2, 'e': 20},
        {'a': 1, 'b': 1, 'd': 0, 'e': 10},
        {'c': 0, 'd': 0},
        {'c': 0, 'd': 2},
    ]))

    e = Experiments()
    e.params(a=[0, 1], b=[0, 1])
    with e.view("True") as v:
        v.params(b=[0, 1, 2], c=[0, 1])
    assert list(e) == list(InstanceGroup([
        {'a': 0, 'b': 0, 'c': 0},
        {'a': 0, 'b': 0, 'c': 1},
        {'a': 0, 'b': 1, 'c': 0},
        {'a': 0, 'b': 1, 'c': 1},
        {'a': 0, 'b': 2, 'c': 0},
        {'a': 0, 'b': 2, 'c': 1},
        {'a': 1, 'b': 0, 'c': 0},
        {'a': 1, 'b': 0, 'c': 1},
        {'a': 1, 'b': 1, 'c': 0},
        {'a': 1, 'b': 1, 'c': 1},
        {'a': 1, 'b': 2, 'c': 0},
        {'a': 1, 'b': 2, 'c': 1},
    ]))


def test_translate():

    e = Experiments()
    e.params(a=range(2), b=range(2), c=range(2))

    assert sorted(e.translate("{{a}}-{{b}}")) == \
        ['0-0', '0-1', '1-0', '1-1']
    assert sorted(e.translate("{{a}}-{{b}}", with_unique=False)) == \
        ['0-0', '0-0', '0-1', '0-1', '1-0', '1-0', '1-1', '1-1']

    assert sorted(e.translate("{{a}}-{{b}}", with_exps=True),
                  key=lambda x: x[0]) == \
        [('0-0', [{'a': 0, 'b': 0, 'c': 0}, {'a': 0, 'b': 0, 'c': 1}]),
         ('0-1', [{'a': 0, 'b': 1, 'c': 0}, {'a': 0, 'b': 1, 'c': 1}]),
         ('1-0', [{'a': 1, 'b': 0, 'c': 0}, {'a': 1, 'b': 0, 'c': 1}]),
         ('1-1', [{'a': 1, 'b': 1, 'c': 0}, {'a': 1, 'b': 1, 'c': 1}])]
    assert sorted(e.translate("{{a}}-{{b}}", with_exps=True, with_unique=False),
                  key=lambda x: x[0]) == \
        [('0-0', [{'a': 0, 'b': 0, 'c': 0}]),
         ('0-0', [{'a': 0, 'b': 0, 'c': 1}]),
         ('0-1', [{'a': 0, 'b': 1, 'c': 0}]),
         ('0-1', [{'a': 0, 'b': 1, 'c': 1}]),
         ('1-0', [{'a': 1, 'b': 0, 'c': 0}]),
         ('1-0', [{'a': 1, 'b': 0, 'c': 1}]),
         ('1-1', [{'a': 1, 'b': 1, 'c': 0}]),
         ('1-1', [{'a': 1, 'b': 1, 'c': 1}])]

    with e.view("c == 0") as v:
        assert sorted(v.translate("{{a}}-{{b}}")) == \
            ['0-0', '0-1', '1-0', '1-1']
        assert sorted(v.translate("{{a}}-{{b}}", with_unique=False)) == \
            ['0-0', '0-1', '1-0', '1-1']

        assert sorted(v.translate("{{a}}-{{b}}", with_exps=True),
                      key=lambda x: x[0]) == \
            [('0-0', [{'a': 0, 'b': 0, 'c': 0}]),
             ('0-1', [{'a': 0, 'b': 1, 'c': 0}]),
             ('1-0', [{'a': 1, 'b': 0, 'c': 0}]),
             ('1-1', [{'a': 1, 'b': 1, 'c': 0}])]
        assert sorted(v.translate("{{a}}-{{b}}", with_exps=True, with_unique=False),
                      key=lambda x: x[0]) == \
            [('0-0', [{'a': 0, 'b': 0, 'c': 0}]),
             ('0-1', [{'a': 0, 'b': 1, 'c': 0}]),
             ('1-0', [{'a': 1, 'b': 0, 'c': 0}]),
             ('1-1', [{'a': 1, 'b': 1, 'c': 0}])]

    def fun(exp):
        return exp['a']+exp['b']
    e.params(c=with_exp(fun))
    assert e.translate("{{c}}") == \
        ['0', '1', '2']
    assert e.translate("{{c}}", with_exps=True) == \
        [('0', [{'a': 0, 'b': 0, 'c': with_exp(fun)}]),
         ('1', [{'a': 0, 'b': 1, 'c': with_exp(fun)}, {'a': 1, 'b': 0, 'c': with_exp(fun)}]),
         ('2', [{'a': 1, 'b': 1, 'c': with_exp(fun)}])]
    with e.view("a == 0") as v:
        assert v.translate("{{c}}") == \
            ['0', '1']
        assert v.translate("{{c}}", with_exps=True) == \
            [('0', [{'a': 0, 'b': 0, 'c': with_exp(fun)}]),
             ('1', [{'a': 0, 'b': 1, 'c': with_exp(fun)}])]

    def fun(exp, arg):
        return "%s:%d" % (arg, exp['a']+exp['b'])
    e.params(c=with_exp_tpl(fun, "{{a}}-{{b}}"))
    assert e.translate("{{c}}") == \
        ['0-0:0', '0-1:1', '1-0:1', '1-1:2']
    assert e.translate("{{c}}", with_exps=True) == \
        [('0-0:0', [{'a': 0, 'b': 0, 'c': with_exp_tpl(fun, "{{a}}-{{b}}")}]),
         ('0-1:1', [{'a': 0, 'b': 1, 'c': with_exp_tpl(fun, "{{a}}-{{b}}")}]),
         ('1-0:1', [{'a': 1, 'b': 0, 'c': with_exp_tpl(fun, "{{a}}-{{b}}")}]),
         ('1-1:2', [{'a': 1, 'b': 1, 'c': with_exp_tpl(fun, "{{a}}-{{b}}")}])]
    with e.view("a == 0") as v:
        assert v.translate("{{c}}") == \
            ['0-0:0', '0-1:1']
        assert v.translate("{{c}}", with_exps=True) == \
            [('0-0:0', [{'a': 0, 'b': 0, 'c': with_exp_tpl(fun, "{{a}}-{{b}}")}]),
             ('0-1:1', [{'a': 0, 'b': 1, 'c': with_exp_tpl(fun, "{{a}}-{{b}}")}])]


def test_execute():
    def read_file(exp, path):
        return open(path, "r").read()

    with tempfile.TemporaryDirectory() as tmp_dir:
        e = Experiments()
        e.params(a=range(2))
        e.execute("echo -n {{a}}{{a}}{{a}}", stdout=os.path.join(tmp_dir, "{{a}}"))
        e.params(f=with_exp_tpl(read_file, os.path.join(tmp_dir, "{{a}}")))
        assert e.translate("{{f}}") == ['000', '111']

    e = Experiments()
    e.params(a=range(2))
    with tempfile.TemporaryDirectory() as tmp_dir, \
         e.view("a == 0") as v:
        v.execute("echo -n {{a}}{{a}}{{a}}", stdout=os.path.join(tmp_dir, "{{a}}"))
        v.params(f=with_exp_tpl(read_file, os.path.join(tmp_dir, "{{a}}")))
        assert v.translate("{{f}}") == ['000']
        with pytest.raises(VariableError):
            e.translate("{{f}}")


def test_find_files():
    def populate(tmp_dir):
        open(os.path.join(tmp_dir, "0-0"), "w")
        open(os.path.join(tmp_dir, "0-1"), "w")
        open(os.path.join(tmp_dir, "1-0"), "w")
        open(os.path.join(tmp_dir, "1-1"), "w")
    def experiments(template=None, path=None):
        e = Experiments()
        e.params(a=range(3))
        if template is None:
            f = None
        else:
            f = from_find_files(os.path.join(tmp_dir, template),
                                path=path, sort=True)
        return e, f

    with tempfile.TemporaryDirectory() as tmp_dir:
        populate(tmp_dir)

        e, f = experiments("{{a}}")
        with pytest.raises(ValueError):
            e.params(f=f.param("unknown"))

        e, f = experiments("{{b}}", path="FILE")
        e.params(f=f.param("FILE"))
        assert list(e.view("a < 2")) == list(InstanceGroup([
            {"a": 0, "f": os.path.join(tmp_dir, "0-0")},
            {"a": 0, "f": os.path.join(tmp_dir, "0-1")},
            {"a": 0, "f": os.path.join(tmp_dir, "1-0")},
            {"a": 0, "f": os.path.join(tmp_dir, "1-1")},
            {"a": 1, "f": os.path.join(tmp_dir, "0-0")},
            {"a": 1, "f": os.path.join(tmp_dir, "0-1")},
            {"a": 1, "f": os.path.join(tmp_dir, "1-0")},
            {"a": 1, "f": os.path.join(tmp_dir, "1-1")},
        ]))

        # eliminated because of unmatch on "a"
        e, f = experiments("{{a}}", path="FILE")
        e.params(f=f.param("FILE"))
        assert list(e) == []

        # align "a" (drops a==2)
        e, f = experiments("{{a}}-{{b}}", path="FILE")
        e.params(f=f.param("FILE"))
        assert list(e) == list(InstanceGroup([
            {"a": 0, "f": os.path.join(tmp_dir, "0-0")},
            {"a": 0, "f": os.path.join(tmp_dir, "0-1")},
            {"a": 1, "f": os.path.join(tmp_dir, "1-0")},
            {"a": 1, "f": os.path.join(tmp_dir, "1-1")},
        ]))

        e, f = experiments("{{a}}-{{b}}", path="FILE")
        e.params(f=f.param("FILE"), b=f.param("b"))
        assert list(e) == list(InstanceGroup([
            {"a": 0, "b": 0, "f": os.path.join(tmp_dir, "0-0")},
            {"a": 0, "b": 1, "f": os.path.join(tmp_dir, "0-1")},
            {"a": 1, "b": 0, "f": os.path.join(tmp_dir, "1-0")},
            {"a": 1, "b": 1, "f": os.path.join(tmp_dir, "1-1")},
        ]))

        e, f = experiments("{{a}}-{{b}}", path="FILE")
        e.params(b=f.param("b"))
        assert list(e) == list(InstanceGroup([
            {"a": 0, "b": 0},
            {"a": 0, "b": 1},
            {"a": 1, "b": 0},
            {"a": 1, "b": 1},
        ]))

        e, f = experiments("{{a}}-{{b}}")
        e.params(b=f.param("b"))
        assert list(e) == list(InstanceGroup([
            {"a": 0, "b": 0},
            {"a": 0, "b": 1},
            {"a": 1, "b": 0},
            {"a": 1, "b": 1},
        ]))

        e = Experiments()
        e.params(a=f.param("a"))
        assert list(e) == list(InstanceGroup([
            {"a": 0},
            {"a": 1},
        ]))

        e = Experiments()
        e.params(a=f.param("a"), b=f.param("b"))
        assert list(e) == list(InstanceGroup([
            {"a": 0, "b": 0},
            {"a": 0, "b": 1},
            {"a": 1, "b": 0},
            {"a": 1, "b": 1},
        ]))

        e, f = experiments("none-{{b}}")
        e.params(b=f.param("b"))
        assert list(e) == list(InstanceGroup([
        ]))


def test_pack():
    def populate(tmp_dir):
        open(os.path.join(tmp_dir, "0-0"), "w").write("")
        open(os.path.join(tmp_dir, "0-1"), "w").write("")
        open(os.path.join(tmp_dir, "1-0"), "w").write("")
        open(os.path.join(tmp_dir, "1-1"), "w").write("")

    e = Experiments()
    e.params(a=range(2), b=range(2))
    with pytest.raises(VariableError):
        e.pack("{{none}}", "{{a}}-{{b}}")
    with pytest.raises(VariableError):
        e.pack("{{a}}-{{b}}", "{{none}}")

    with tempfile.TemporaryDirectory() as tmp_dir:
        populate(tmp_dir)
        e = Experiments(out=os.path.join(tmp_dir, "out"))
        e.params(a=range(2), b=range(2))
        e.pack(os.path.join(tmp_dir, "{{a}}-{{b}}"), "{{a}}-{{b}}")
        out_files = utils.find_files(
            os.path.join(tmp_dir, "out", "{{a}}-{{b}}"),
            path="PATH")
        assert list(out_files) == list(InstanceGroup([
            {"PATH": os.path.join(tmp_dir, "out", "0-0"), "a": 0, "b": 0},
            {"PATH": os.path.join(tmp_dir, "out", "0-1"), "a": 0, "b": 1},
            {"PATH": os.path.join(tmp_dir, "out", "1-0"), "a": 1, "b": 0},
            {"PATH": os.path.join(tmp_dir, "out", "1-1"), "a": 1, "b": 1},
        ]))

    with tempfile.TemporaryDirectory() as tmp_dir:
        populate(tmp_dir)
        e = Experiments(out=os.path.join(tmp_dir, "out"))
        e.params(a=range(2), b=range(2))
        e.pack(os.path.join(tmp_dir, "{{a}}-{{b}}"), "{{a}}-1")
        out_files = utils.find_files(
            os.path.join(tmp_dir, "out", "{{a}}-{{b}}"),
            path="PATH")
        assert list(out_files) == list(InstanceGroup([
            {"PATH": os.path.join(tmp_dir, "out", "0-1"), "a": 0, "b": 1},
            {"PATH": os.path.join(tmp_dir, "out", "1-1"), "a": 1, "b": 1},
        ]))

    with tempfile.TemporaryDirectory() as tmp_dir:
        populate(tmp_dir)
        e = Experiments(out=os.path.join(tmp_dir, "out"))
        e.params(a=range(2), b=range(2))
        e.pack(os.path.join(tmp_dir, "{{a}}-1"), "{{a}}-{{b}}")
        out_files = utils.find_files(
            os.path.join(tmp_dir, "out", "{{a}}-{{b}}"),
            path="PATH")
        assert list(out_files) == list(InstanceGroup([
            {"PATH": os.path.join(tmp_dir, "out", "0-0"), "a": 0, "b": 0},
            {"PATH": os.path.join(tmp_dir, "out", "0-1"), "a": 0, "b": 1},
            {"PATH": os.path.join(tmp_dir, "out", "1-0"), "a": 1, "b": 0},
            {"PATH": os.path.join(tmp_dir, "out", "1-1"), "a": 1, "b": 1},
        ]))

    with tempfile.TemporaryDirectory() as tmp_dir:
        populate(tmp_dir)
        e = Experiments(out=os.path.join(tmp_dir, "out"))
        e.params(a=range(2), b=range(3))
        with pytest.raises(OSError):
            e.pack(os.path.join(tmp_dir, "{{a}}-{{b}}"), "{{a}}-{{b}}")


def test_generate():
    def populate(tmp_dir):
        open(os.path.join(tmp_dir, "a0"), "w").write("a0_f {{b}}")
        open(os.path.join(tmp_dir, "a1"), "w").write("a1_f {{b}}")

    e = Experiments()
    e.params(a=range(2), b=range(2))
    with pytest.raises(VariableError):
        e.generate("{{none}}", "{{a}}-{{b}}")
    with pytest.raises(VariableError):
        e.generate("{{a}}", "{{none}}")

    with tempfile.TemporaryDirectory() as tmp_dir:
        populate(tmp_dir)
        with pytest.raises(FileNotFoundError):
            e.generate(os.path.join(tmp_dir, "{{a}}"), "{{a}}")

    with tempfile.TemporaryDirectory() as tmp_dir:
        populate(tmp_dir)
        e = Experiments(out=os.path.join(tmp_dir, "out"))
        e.params(a=["a0", "a1"], b=["b0", "b1"])
        e.generate(os.path.join(tmp_dir, "{{a}}"), "{{a}}-{{b}}")
        out_files = utils.find_files(
            os.path.join(tmp_dir, "out", "{{a}}-{{b}}"),
            path="PATH")
        assert list(out_files) == list(InstanceGroup([
            {"PATH": os.path.join(tmp_dir, "out", "a0-b0"), "a": "a0", "b": "b0"},
            {"PATH": os.path.join(tmp_dir, "out", "a0-b1"), "a": "a0", "b": "b1"},
            {"PATH": os.path.join(tmp_dir, "out", "a1-b0"), "a": "a1", "b": "b0"},
            {"PATH": os.path.join(tmp_dir, "out", "a1-b1"), "a": "a1", "b": "b1"},
        ]))

    with tempfile.TemporaryDirectory() as tmp_dir:
        populate(tmp_dir)
        e = Experiments(out=os.path.join(tmp_dir, "out"))
        e.params(a=["a0", "a1"], b=["b0", "b1"])
        e.generate(os.path.join(tmp_dir, "{{a}}"), "{{a}}")
        out_files = utils.find_files(
            os.path.join(tmp_dir, "out", "{{a}}"),
            path="PATH")
        assert list(out_files) == list(InstanceGroup([
            {"PATH": os.path.join(tmp_dir, "out", "a0"), "a": "a0"},
            {"PATH": os.path.join(tmp_dir, "out", "a1"), "a": "a1"},
        ]))
        with open(os.path.join(tmp_dir, "out", "a0"), "r") as f:
            assert f.read() == "a0_f b0"
        with open(os.path.join(tmp_dir, "out", "a1"), "r") as f:
            assert f.read() == "a1_f b0"


def test_generate_jobs():
    with tempfile.TemporaryDirectory() as tmp_dir:
        e = Experiments(out=os.path.join(tmp_dir, "out"))
        e.params(a=range(2))

        with pytest.raises(TemplateError):
            e.generate_jobs("none", "jobs/{{a}}")

        with pytest.raises(ValueError):
            e.generate_jobs("shell", "jobs/{{a}}", export=["b"])

        ee = Experiments(e)
        ee.params(LAUNCHER=1)
        with pytest.raises(ValueError):
            ee.generate_jobs("shell", "jobs/{{a}}")

        with pytest.raises(TypeError):
            e.generate_jobs("shell", "jobs/{{a}}", export=1)
        with pytest.raises(TypeError):
            e.generate_jobs("shell", "jobs/{{a}}", export=[1])
        with pytest.raises(ValueError):
            e.generate_jobs("shell", "jobs/{{a}}", export=["b"])

        e = Experiments(e)
        with pytest.raises(TypeError):
            e.generate_jobs("shell", "jobs/{{a}}", depends=1)
        with pytest.raises(TypeError):
            e.generate_jobs("shell", "jobs/{{a}}", depends=[1])

        with pytest.raises(ValueError):
            e.generate_jobs("shell", "jobs/{{a}}", job_desc="/tmp/jobs.jd")

        with pytest.raises(VariableError):
            e.generate_jobs("shell", "jobs/{{b}}")

        with pytest.raises(VariableError):
            e.generate_jobs("shell", "jobs/{{a}}")

        e.params(CMD="echo {{a}}")
        e.generate_jobs("shell", "jobs/{{a}}", export=["a"], depends=["deps/{{a}}"])
