#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#pylint: skip-file

"""Filter tests."""

__author__ = "Lluís Vilanova"
__copyright__ = "Copyright 2013-2023, Lluís Vilanova"
__license__ = "GPL version 3 or later"


import pytest

from sciexp2.common.filter import *



def test_match():
    assert Filter(dict(a=1)).match(dict(a=1))
    assert Filter("a == 1").match(dict(a=1))

    assert not Filter(dict(a=1)).match(dict(a=2))
    assert not Filter("a == 1").match(dict(a=2))

    with pytest.raises(NameError):
        Filter("a == 1").match(dict(b=1))


def test_allowed():
    Filter(dict(a=1)).validate("a")

    with pytest.raises(NameError):
        Filter(dict(a=1)).validate("b")

    Filter("a == 1").validate("a")

    with pytest.raises(NameError):
        Filter("a == 1").validate("b")


def test_merge():
    f1 = and_filters(Filter("a == 1"), Filter("b == 2"))
    f2 = Filter("(a == 1) and (b == 2)")
    assert str(f1) == str(f2)

    f1 = and_filters(Filter(dict(a=1)), Filter(dict(b=2)))
    f2 = Filter("(a == 1) and (b == 2)")
    assert str(f1) == str(f2)


def test_re_match():
    with pytest.raises(TypeError):
        assert Filter("re_match(111, '1.*')").match(dict())
    assert Filter("re_match('111', '1.*')").match(dict())
    assert Filter("re_match('111', '111')").match(dict())
    assert not Filter("re_match('111', '2.*')").match(dict())
    assert not Filter("re_match('111', '211')").match(dict())
