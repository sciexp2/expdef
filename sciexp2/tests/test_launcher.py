#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#pylint: skip-file

__author__ = "Lluís Vilanova"
__copyright__ = "Copyright 2017-2023, Lluís Vilanova"
__license__ = "GPL version 3 or later"


import os
from sciexp2.common.instance import InstanceGroup
from sciexp2.expdef.env import *
from sciexp2.expdef import launcher
import tempfile


def test_launcher():
    with tempfile.TemporaryDirectory() as tmp_dir:
        e = Experiments(out=os.path.join(tmp_dir, "out"))
        e.params(a=range(2), CMD="echo {{a}}")

        e.generate_jobs("shell", "jobs/{{a}}", export=["a"], depends=["deps/{{a}}"])
        os.mkdir(os.path.join(tmp_dir, "out", "deps"))

        # check state before run
        jobs = launcher.load(os.path.join(tmp_dir, "out", "jobs.jd"))
        jobs_system = jobs._system.build([])
        assert all(lambda job: job["_STATE"] == "notrun"
                   for job in jobs_system)

        # check state after run (missing deps)
        jobs.submit([])
        jobs_system = jobs._system.build([])
        for job, val in zip(jobs_system, range(len(e))):
            assert job["_STATE"] == "outdated"
            with open(os.path.join(e.out, job["DONE"]), "r") as f:
                f.read() == str(val)

        # check state after run (pre-generate deps)
        open(os.path.join(tmp_dir, "out", "deps", "0"), "w")
        open(os.path.join(tmp_dir, "out", "deps", "1"), "w")
        jobs.submit([])
        jobs_system = jobs._system.build([])
        for job in jobs_system:
            assert job["_STATE"] == "done"

        # check state after updating dep
        res_path = os.path.join(tmp_dir, "out", "out", "jobs", "0.stdout")
        res_mtime = os.stat(res_path).st_mtime
        os.utime(res_path, (res_mtime-100, res_mtime-100))
        jobs_system = InstanceGroup(jobs._system.build([]))
        assert list(jobs_system.select("a == 0"))[0]["_STATE"] == "outdated"
        assert list(jobs_system.select("a == 1"))[0]["_STATE"] == "done"

        # check state after run (all in place)
        jobs.submit([])
        jobs_system = jobs._system.build([])
        for job in jobs_system:
            assert job["_STATE"] == "done"
