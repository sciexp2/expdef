#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#pylint: skip-file

"""Text translation/extraction tests."""

__author__ = "Lluís Vilanova"
__copyright__ = "Copyright 2013-2024, Lluís Vilanova"
__license__ = "GPL version 3 or later"


import pystache.parser
import pytest
from sciexp2.common.text import *
from sciexp2.common.instance import Instance


def test_parse_verify():
    # allowed tags
    translate("{{a}}", {'a':1})
    translate("{{#a}}{{/a}}", {'a':1})
    translate("{{^a}}{{/a}}", {'a':1})
    translate("{{=<% %>=}}<%a%><%={{ }}=%>{{a}}", {'a':1})

    # unbalanced tags
    with pytest.raises(ParseError):
        translate("{{#a}}", {'a':1})
    with pytest.raises(ParseError):
        translate("{{/a}}", {'a':1})
    with pytest.raises(ParseError):
        translate("{{>a}}", {'a':1})

    # nested
    with pytest.raises(ParseError):
        extract("{{#a}} {{b}} {{/a}}", "")


def test_get_variables():
    text = "a {{var1}} b {{#var2}} c {{var3}} d {{/var2}} e {{^var4}} f {{var5}} g {{/var4}} h"

    with pytest.raises(ParseError):
        get_variables(text)
    assert get_variables(text, nested="inner") == ["var1", "var3", "var5"]
    assert get_variables(text, nested="all") == ["var1", "var2", "var3", "var4", "var5"]


def test_translate():
    with pytest.raises(VariableError):
        translate("{{var}}", {})
    assert translate("a {{var}} b", {'var': 'var'}) == "a var b"

    with pytest.raises(VariableError):
        translate("{{#var}}{{var}}", {})
    with pytest.raises(VariableError):
        translate("{{^var}}{{var}}", {})

    assert translate("{{#var1}}a{{/var1}}", {'var1': True}) == "a"
    assert translate("{{#var1}}a{{/var1}}", {'var1': False}) == ""
    assert translate("{{^var1}}a{{/var1}}", {'var1': True}) == ""
    assert translate("{{^var1}}a{{/var1}}", {'var1': False}) == "a"

    assert translate("{{#var1}}{{var2}}{{/var1}}", {'var1': []}) == ""
    assert translate("{{#var1}}{{var2}}{{/var1}}", {'var1': [{'var2':1}, {'var2':2}], 'var2': 3}) == "12"

    assert translate("{{#var1}}a{{/var1}}", {'var1': []}) == ""
    assert translate("{{#var1}}a{{/var1}}", {'var1': [1]}) == "a"
    assert translate("{{^var1}}a{{/var1}}", {'var1': []}) == "a"
    assert translate("{{^var1}}a{{/var1}}", {'var1': [1]}) == ""

    assert translate("{{var}}", {'var': "{{a}}", 'a': 'a'}, recursive=False) == "{{a}}"
    assert translate("{{var}}", {'var': "{{a}}", 'a': 'a'}, recursive=True) == "a"
    with pytest.raises(VariableError):
        translate("{{var}}", {'var': "{{a}}"}, recursive=True)

    assert translate("{{var.var}}", {'var': {'var': 1}}) == "1"
    with pytest.raises(VariableError):
        translate("{{var.var}}", {'var': {}})

    assert translate("{{var}}", {'var': lambda: "value"}) == "value"
    with pytest.raises(TypeError):
        translate("{{var}}", {'var': lambda arg: "value"})
    assert translate("{{#var1}}{{var2}}{{/var1}}",
                     {'var1': lambda arg: "-%s-" % arg, 'var2': "value"}) == "-value-"
    with pytest.raises(VariableError):
        translate("{{#var1}}{{var2}}{{/var1}}", {'var1': lambda arg: "-%s-" % arg})
    assert translate("{{#var1}}{{var2}}{{/var1}}",
                     {'var1': lambda arg: 2*arg, 'var2': 1}) == "11"

def test_auto_translate():
    env = Instance({'a': 1, 'b': '{{c}}'})

    assert env['a'] == 1
    assert env['b'] == '{{c}}'

    with env.auto_translate():
        assert env['a'] == 1
        assert env['b'] == '{{c}}'

    env['c'] = 2

    assert env['a'] == 1
    assert env['b'] == '{{c}}'

    with env.auto_translate():
        assert env['a'] == 1
        assert env['b'] == '2'

def test_translate_many():
    envs = [{'a': 1}, {'a': 2, 'b': 1}, {'a': 2, 'b': 2}]

    assert translate_many("{{a}}", envs) == ['1', '2']
    assert translate_many("{{a}}", envs, with_envs=True) == [('1', [{'a': 1}]),
                                                             ('2', [{'a': 2, 'b': 1}, {'a': 2, 'b': 2}])]

    with pytest.raises(VariableError):
        translate_many("{{a}}", [{'a': 1}, {}])
    assert translate_many("{{a}}", [{'a': 1}, {}], ignore_variable_error=True) == ["1"]


def test_extract():
    with pytest.raises(ParseError):
        extract("{{#a}}{{/a}}", "")
    with pytest.raises(ParseError):
        extract("{{^a}}{{/a}}", "")
    with pytest.raises(ParseError):
        extract("{{a}}{{#a}}text{{/a}}", "")

    with pytest.raises(ExtractError):
        extract("a", "")
    with pytest.raises(ExtractError):
        extract("{{a}}", "")
    assert extract("a", "a") == {}
    assert extract("{{a}}", "a") == {'a': 'a'}
    assert extract("{{a}}?", "") == {'a': None}
    assert extract("{{#a}}[0-9]+{{/a}}?", "") == {'a': None}
    assert extract("{{#a}}[0-9]+{{/a}}.*{{a}}", "123-123") == {'a': '123'}
    assert extract("{{#a}}[0-9]+{{/a}}.*{{a}}{{b}}", "123-123456") == \
        {'a': '123', 'b': '456'}
