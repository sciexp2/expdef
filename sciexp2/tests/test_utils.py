#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#pylint: skip-file

__author__ = "Lluís Vilanova"
__copyright__ = "Copyright 2019-2023, Lluís Vilanova"
__license__ = "GPL version 3 or later"


import pytest
from sciexp2.common.instance import *
from sciexp2.common.utils import *
import tempfile


def test_find_files():
    with pytest.raises(ValueError):
        find_files("{{a}}", path="a")

    def populate(dir):
        open(os.path.join(dir, "3-3.txt"), "w")
        open(os.path.join(dir, "2-2"), "w")
        open(os.path.join(dir, "2-1"), "w")
        open(os.path.join(dir, "1-2"), "w")
        open(os.path.join(dir, "1-1"), "w")

    with tempfile.TemporaryDirectory() as tmp_dir:
        populate(tmp_dir)
        dir = os.path.join(tmp_dir, "a")
        os.mkdir(dir)
        populate(dir)
        dir = os.path.join(tmp_dir, "b")
        os.mkdir(dir)
        populate(dir)

        assert list(find_files(os.path.join(tmp_dir, "{{a}}/"))) == list(InstanceGroup(
            [{'a': 'a'}, {'a': 'b'}]))
        assert list(find_files(os.path.join(tmp_dir, "{{a}}/"), path="FILE")) == list(InstanceGroup(
            [{'a': 'a', 'FILE': os.path.join(tmp_dir, "a")},
             {'a': 'b', 'FILE': os.path.join(tmp_dir, "b")}]))

        assert list(find_files(os.path.join(tmp_dir, "{{a}}/{{b}}/"))) == []

        assert list(find_files(os.path.join(tmp_dir, "{{a}}/{{b}}"))) == list(InstanceGroup(
            [{'a': 'a', 'b': '1-1'}, {'a': 'a', 'b': '1-2'},
             {'a': 'a', 'b': '2-1'}, {'a': 'a', 'b': '2-2'},
             {'a': 'a', 'b': '3-3.txt'},
             {'a': 'b', 'b': '1-1'}, {'a': 'b', 'b': '1-2'},
             {'a': 'b', 'b': '2-1'}, {'a': 'b', 'b': '2-2'},
             {'a': 'b', 'b': '3-3.txt'}]))

        assert list(find_files(os.path.join(tmp_dir, "{{a}}/{{b}}.txt"))) == list(InstanceGroup(
            [{'a': 'a', 'b': '3-3'},
             {'a': 'b', 'b': '3-3'}]))

        assert list(find_files(os.path.join(tmp_dir, "{{a}}/{{#b}}.*[0-9]{{/b}}"))) == list(InstanceGroup(
            [{'a': 'a', 'b': '1-1'}, {'a': 'a', 'b': '1-2'},
             {'a': 'a', 'b': '2-1'}, {'a': 'a', 'b': '2-2'},
             {'a': 'b', 'b': '1-1'}, {'a': 'b', 'b': '1-2'},
             {'a': 'b', 'b': '2-1'}, {'a': 'b', 'b': '2-2'}]))

        assert list(find_files(os.path.join(tmp_dir, "{{a}}/{{b}}-{{#c}}[0-9]+{{/c}}"))) == list(InstanceGroup(
            [{'a': 'a', 'b': 1, 'c': 1}, {'a': 'a', 'b': 1, 'c': 2},
             {'a': 'a', 'b': 2, 'c': 1}, {'a': 'a', 'b': 2, 'c': 2},
             {'a': 'b', 'b': 1, 'c': 1}, {'a': 'b', 'b': 1, 'c': 2},
             {'a': 'b', 'b': 2, 'c': 1}, {'a': 'b', 'b': 2, 'c': 2}]))
